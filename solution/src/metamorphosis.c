#include "metamorphosis.h"

struct pixel img_get_pixel(struct image const* source, uint64_t x, uint64_t y) {
    return (source->data[y * source->width + x]);
}

void img_set_pixel(struct image* img, struct pixel const pixel, uint64_t x, uint64_t y) {
    img->data[y * img->width + x] = pixel;
}

struct image rotate(struct image const* source){
    struct image img = image_create(source->height, source->width);
    for (uint64_t i = 0; i < img.width; i++){
        for (uint64_t j = 0; j < img.height; j++){
            img_set_pixel(&img, img_get_pixel(source, j, img.width - i - 1), i, j);
        }
    }
    return img;
}


