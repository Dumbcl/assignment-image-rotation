#include "inside_image_format.h"
#include "stddef.h"

struct image image_create(uint64_t width, uint64_t height){
    struct image img;
    img.width = width;
    img.height = height;
    img.data = malloc(sizeof(struct pixel) * width * height);
    return img;
}

void image_destroy(struct image* img){
    free(img->data);
}


