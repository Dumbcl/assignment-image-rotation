#include "input_bmp_format.h"
#include "bmp_header.h"

#define HEADER_BF_TYPE 0x4d42
#define HEADER_BIT_COUNT 24
#define HEADER_OFF_BITS 54
#define HEADER_SIZE 40
#define HEADER_PLANES 1
#define HEADER_COMPRESSION 0
#define HEADER_RESERVED 0

static int64_t get_padding(int64_t width){
    return (4 - ((width * 3) % 4));
}

enum read_status header_validation(const size_t k, struct bmp_header header, struct image* img, FILE* in){
    if (k != 1) {free(img); return READ_INVALID_HEADER;}
    if (header.bfType != HEADER_BF_TYPE) {free(img); return READ_INVALID_SIGNATURE;}
    if (header.biBitCount != HEADER_BIT_COUNT) {free(img); return READ_INVALID_BITS;}
    struct image cool_image = image_create(header.biWidth, header.biHeight);
    const int64_t img_padding = get_padding((int64_t)cool_image.width);;
    for (uint64_t i = 0; i < cool_image.height; i++) {
        size_t pixels_current_row = fread(cool_image.data + cool_image.width * i, sizeof(struct pixel), cool_image.width, in);
        if (pixels_current_row != cool_image.width) {free(img); return READ_UNEXPECTED_ERROR;}
        if (fseek(in, img_padding, SEEK_CUR) != 0) {free(img); return READ_UNEXPECTED_ERROR;}
    }
    *img = cool_image;
    return READ_OK;
}

struct bmp_header header_values_fill(struct image const* img, const int64_t img_padding){
    struct bmp_header header = {0};
    header.bfType = HEADER_BF_TYPE;
    header.bOffBits = HEADER_OFF_BITS;
    header.biSize = HEADER_SIZE;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = HEADER_PLANES;
    header.biBitCount = HEADER_BIT_COUNT;
    header.biCompression = HEADER_COMPRESSION;
    header.biSizeImage = (img->width * sizeof(struct pixel) + img_padding) * img->height;
    header.bfileSize = header.biSizeImage + sizeof(struct bmp_header);
    header.bfReserved = HEADER_RESERVED;
    return header;
}

enum read_status from_bmp(FILE* in, struct image* img){
    struct bmp_header header = {0};
    const size_t k = fread((&header), sizeof(struct bmp_header), 1, in);
    return header_validation(k, header, img, in);
}

enum write_status to_bmp(FILE* out, struct image const* img){
    const int64_t img_padding = get_padding((int64_t)img->width);
    struct bmp_header header = header_values_fill(img, img_padding);
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)){
        return WRITE_ERROR;
    }
    struct pixel* pixels = img->data;
    for (uint64_t i = 0; i < img->height; i++){
        if (!fwrite(img->data + i * img->width, sizeof(struct pixel) * img->width, 1, out)) return WRITE_ERROR;
        if (!fwrite(pixels, img_padding, 1, out) && img_padding != 0) return WRITE_ERROR;
    }
    return WRITE_OK;
}


