#include "open_close_file.h"

enum open_status file_open(FILE** file, const char* path, char* mode){
    *file = fopen(path, mode);
    if (*file != 0) return OPEN_OK; else return OPEN_UNEXPECTED_ERROR;
}

enum close_status file_close(FILE* file){
    size_t cs = fclose(file);
    if (cs == 0) return CLOSE_OK; else return CLOSE_UNEXPECTED_ERROR;
}

