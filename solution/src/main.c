#include "metamorphosis.h"
#include "input_bmp_format.h"
#include "open_close_file.h"

void complete_destroy(struct image* img, struct image* new_img){
    image_destroy(img);
    free(img);
    image_destroy(new_img);
    free(new_img);
}

int main(int argc, char** argv) {
    if (argc != 3) {
        fprintf(stderr, "Incorrect number of arguments");
        return 1;
    }
    FILE *frptr;
    FILE** file_read = &(frptr);
    enum open_status file_read_open_status = file_open(file_read, argv[1], "r");
    if (file_read_open_status == OPEN_OK ) fprintf(stderr, "Ok open file to read\n"); else{
        fprintf(stderr, "Error open file to read\n");
        return 1;
    }
    FILE *foptr;
    FILE** file_write = &(foptr);
    enum open_status file_write_open_status = file_open(file_write, argv[2], "w");
    if (file_write_open_status == OPEN_OK) fprintf(stderr, "Ok open file to write\n"); else{
        fprintf(stderr, "Error open file to write\n");
        return 1;
    }
    struct image* img = malloc(sizeof(struct image));
    enum read_status file_read_read_status = from_bmp(*file_read, img);
    switch (file_read_read_status){
        case READ_OK:
            fprintf(stderr, "Ok read file\n");
            break;
        case READ_INVALID_SIGNATURE:
            fprintf(stderr, "Error with signature\n");
            return 1;
        case READ_INVALID_BITS:
            fprintf(stderr, "Error with bits\n");
            return 1;
        case READ_INVALID_HEADER:
            fprintf(stderr, "Error with header\n");
            return 1;
        case READ_UNEXPECTED_ERROR:
            fprintf(stderr, "Error unexpected\n");
            return 1;
    }
    struct image* new_img = malloc(sizeof(struct image));
    *new_img = rotate(img);
    enum write_status file_write_write_status = to_bmp( *file_write, new_img );
    if (file_write_write_status == WRITE_OK ) fprintf(stderr, "Ok write file\n"); else {
        complete_destroy(img, new_img);
        fprintf(stderr, "Error write file");
        return 1;
    }
    enum close_status file_read_close_status = file_close(*file_read);
    if (file_read_close_status == CLOSE_OK) fprintf(stderr, "Ok close file to read\n"); else{
        complete_destroy(img, new_img);
        fprintf(stderr, "Error close file to read\n");
        return 1;
    }
    enum close_status file_write_close_status = file_close(*file_write);
    if (file_write_close_status == CLOSE_OK) fprintf(stderr, "Ok close file to write\n"); else{
        complete_destroy(img, new_img);
        fprintf(stderr, "Error close file to write\n");
        return 1;
    }
    complete_destroy(img, new_img);
    return 0;
}
