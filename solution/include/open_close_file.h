#ifndef OPEN_CLOSE_FILE_H
#define OPEN_CLOSE_FILE_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

enum open_status {
    OPEN_OK = 0,
    OPEN_ALREADY_OPENED,
    OPEN_UNEXPECTED_ERROR
};

enum open_status file_open(FILE** file, const char* path, char* mode);

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ALREADY_CLOSED,
    CLOSE_UNEXPECTED_ERROR
};

enum close_status file_close(FILE* file);


#endif
