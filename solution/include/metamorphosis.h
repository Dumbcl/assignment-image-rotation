#ifndef METAMORPHOSIS_H
#define METAMORPHOSIS_H

#include "inside_image_format.h"

struct image rotate(struct image const* source);

#endif
