#ifndef INSIDE_IMAGE_FORMAT_H
#define INSIDE_IMAGE_FORMAT_H

#include <stdint.h>
#include <stdlib.h>

struct image{
    uint64_t width, height;
    struct pixel* data;
};

struct __attribute__((packed)) pixel{
    uint8_t b, g, r;
};

struct image image_create(uint64_t width, uint64_t height);

void image_destroy(struct image* img);


#endif
